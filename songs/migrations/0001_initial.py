# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Artist',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Song',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=200)),
                ('song_key', models.CharField(max_length=2, choices=[(b'C', b'C'), (b'C#', b'C#'), (b'D', b'D'), (b'D#', b'D#'), (b'E', b'E'), (b'F', b'F'), (b'F#', b'F#'), (b'G', b'G'), (b'G#', b'G#'), (b'A', b'A'), (b'A#', b'A#'), (b'B', b'B')])),
                ('body', models.TextField()),
                ('artist', models.ForeignKey(to='songs.Artist')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
