from django.contrib import admin
from .models import Song, Artist, Tag


class SongAdmin(admin.ModelAdmin):
    list_display = 'title', 'artist', 'song_key'
    fields = ['title', 'artist', 'song_key', 'body', 'tags', 'user']
    search_fields = ['title']

class ArtistAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('name', )}
    search_fields = ['name']


class TagAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('name', )}
    search_fields = ['name']


admin.site.register(Song, SongAdmin)
admin.site.register(Artist, ArtistAdmin)
admin.site.register(Tag, TagAdmin)