# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('songs', '0004_auto_20141110_0835'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='song',
            name='tag',
        ),
        migrations.RemoveField(
            model_name='song',
            name='tags',
        ),
    ]
