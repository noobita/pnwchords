from django.conf.urls import patterns, include, url
from django.contrib.auth.decorators import login_required

from songs import views

urlpatterns = patterns('',
        #url(r'^(?P<slug>[\w|\-]+)/$', views.SongDetailView.as_view(), name='song_detail'),
        url(r'^$', views.SongIndex.as_view(), name='song_index'),
        
        url(r'^add/$', login_required(views.SongAdd.as_view()), name='song_add'),
        url(r'^add-artist/$', login_required(views.ArtistAdd.as_view()), name='artist_add'),
        url(r'^add-tag/$', login_required(views.TagAdd.as_view()), name='tag_add'),
        url(r'^(?P<slug>[\w|\-]+)/$', views.SongDetail.as_view(), name='song_detail'),

        #url(r'^artist/', views.ArtistIndex.as_view(), name='artist_index'),
        url(r'^artists/(?P<slug>[\w|\-]+)/$', views.ArtistDetailView.as_view(), name='artist_detail'),
        #url(r'^users/(?P<slug>[\w|\-]+)/$', views.UserDetail.as_view(), name='user_detail'),
        url(r'^tags/(?P<slug>[\w|\-]+)/$', views.TagDetailView.as_view(), name='tag_detail'),


)
