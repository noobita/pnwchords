from django import forms
from django.forms.models import inlineformset_factory
from .models import Song, Artist, Tag


class SongForm(forms.ModelForm):
	class Meta:
		model = Song
		fields =['title', 'artist', 'song_key', 'body', 'tags']



class ArtistForm(forms.ModelForm):
	class Meta:
		model = Artist
		fields = ['name']

class TagForm(forms.ModelForm):
	class Meta:
		model = Tag
		fields = ['name']

			


SongFormSet = inlineformset_factory(Artist, Song)