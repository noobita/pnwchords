# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('songs', '0006_auto_20141110_0841'),
    ]

    operations = [
        migrations.AlterField(
            model_name='song',
            name='slug',
            field=models.CharField(unique=True, max_length=100),
        ),
    ]
