#from django.shortcuts import render, get_object_or_404, render_to_response
#from django.contrib.auth.decorators import login_required
from django.views import generic
from django.http import HttpResponse, HttpResponseRedirect
#from django.template import Context, loader, RequestContext
from django.shortcuts import render

from .forms import SongForm, ArtistForm, TagForm, SongFormSet

from .models import Song, Artist, Tag
from django.contrib.auth.models import User

def transpose(request):
    pop_songs = Song.objects.order_by('-views')[:10]
    key = request.GET.get('key')
    body = request.GET.get('body')
    context = {'pop_songs': pop_songs, 'key':key, 'body':body}
    return render(request, 'song/transpose.html', context)

class HomeIndex(generic.ListView):
    template_name = 'song/home.html'
    context_object_name = 'songs'

    def get_queryset(self):
        return Song.objects.order_by('-created_at')[:10]

    def get_context_data(self, **kwargs):
        context = super(HomeIndex, self).get_context_data(**kwargs)
        context['pop_songs']= Song.objects.order_by('-views')[:10]
        return context

class SongIndex(generic.ListView):
    template_name = 'song/index.html'
    context_object_name = 'songs'
    paginate_by = 15

    def get_queryset(self):
        q = self.request.GET.get('q')
        key = self.request.GET.get('key')
        if q:
            return Song.objects.filter(title__istartswith=q)
        elif key:
            return Song.objects.filter(song_key__exact=key)
        else:
            return Song.objects.order_by('title')

    def get_context_data(self, **kwargs):
        context = super(SongIndex, self).get_context_data(**kwargs)
        context['pop_songs']= Song.objects.order_by('-views')[:10]
        return context



class SongDetail(generic.DetailView):
    model = Song
    template_name = 'song/song.html'

    def get_context_data(self, **kwargs):
        context = super(SongDetail, self).get_context_data(**kwargs)
        context['pop_songs']= Song.objects.order_by('-views')[:10]
        self.object.add_view_count()
        self.object.save()
        return context


class SongAdd(generic.CreateView):
    template_name= 'song/add.html'
    model = Song
    form_class = SongForm
    success_url = '/'


    def form_valid(self, form):
        form.instance.user = self.request.user
        return super(SongAdd, self).form_valid(form)


class SongUpdate(generic.UpdateView):
    model = Song
    form_class = SongForm
    template_name = 'song/update.html'
    success_url = '/'

    # def get_context_data(self, **kwargs):
    #     context = super(SongUpdate, self).get_context_data(**kwargs)
    #     user = self.request.user
    #     context['songs']= Song.objects.filter(user=user)
    #     return context
    def form_valid(self, form):
        form.instance.user = self.request.user
        return super(SongUpdate, self).form_valid(form)


class ArtistIndex(generic.ListView):
    template_name = 'artist/index.html'
    context_object_name = 'artists'
    paginate_by = 15

    def get_queryset(self):
        return Artist.objects.order_by('slug')

class ArtistDetailView(generic.DetailView):
    model = Artist
    template_name = 'artist/artist.html'
    #context_object_name = 'artists'
    paginate_by = 15


class ArtistAdd(generic.CreateView):
    template_name= 'song/add_artist.html'
    model = Artist
    form_class = ArtistForm
    success_url = '/songs/add/'


class TagDetailView(generic.DetailView):
    model = Tag
    template_name = 'tag/index.html'
    paginate_by = 15


class TagAdd(generic.CreateView):
    template_name= 'song/add_tag.html'
    model = Tag
    form_class = TagForm
    success_url = '/songs/add/'


class Search(generic.ListView):
    template_name = 'search/results.html'
    context_object_name = 'songs'

    def get_queryset(self):
        return Song.objects.order_by('-created_at')[:15]

    def get_context_data(self, **kwargs):
        context = super(Search, self).get_context_data(**kwargs)
        q = self.request.GET['q']
        context['songs']= Song.objects.filter(title__icontains=q)
        context['q'] = q
        return context


class UserDetail(generic.ListView):
    model = User
    template_name = 'user/index.html'
    # context_object_name = 'user'


    # def get_queryset(self):
    #     return Song.objects.order_by('username')
