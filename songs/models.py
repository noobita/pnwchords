from django.db import models
from uuslug import uuslug
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User


class Tag(models.Model):
    name = models.CharField(max_length=200, unique=True)
    slug = models.CharField(max_length=200, blank=True)

    class Meta:
        ordering = ["name"]

    def __str__(self):
        return self.slug

    def save(self, *args, **kwargs):
        self.slug = uuslug(self.name, instance=self, max_length=100)
        super(Tag, self).save(*args, **kwargs)

    def clean(self):
        self.name = self.name.lower()


class Artist(models.Model):
    name = models.CharField(max_length=100, unique=True)
    slug = models.CharField(max_length=100)

    class Meta:
        ordering = ["name"]

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.slug = uuslug(self.name, instance=self, max_length=100)
        super(Artist, self).save(*args, **kwargs)

    # def clean(self):
    #     self.name = self.name.capitalize()


class Song(models.Model):
    S_KEYS = (
        ('C', 'C'),
        ('C#', 'C#'),
        ('D', 'D'),
        ('D#', 'D#'),
        ('E', 'E'),
        ('F', 'F'),
        ('F#', 'F#'),
        ('G', 'G'),
        ('G#', 'G#'),
        ('A', 'A'),
        ('A#', 'A#'),
        ('B', 'B'),
    )
    title = models.CharField(max_length=200)
    artist = models.ForeignKey(Artist)
    user = models.ForeignKey(User)
    song_key = models.CharField(max_length=2, choices=S_KEYS)
    body = models.TextField()
    views = models.IntegerField(default=0)
    slug = models.CharField(max_length=100, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    tags = models.ManyToManyField(Tag, blank=True)

    class Meta:
        ordering = ["title"]

    def __str__(self):
        return self.title


    def save(self, *args, **kwargs):
        self.slug = uuslug(self.title, instance=self, max_length=100)
        super(Song, self).save(*args, **kwargs)


    def add_view_count(self):
        if self.views is not None:
            self.views +=1

    # def clean(self):
    #     self.title = self.title.capitalize()



