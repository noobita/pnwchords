from django.conf import settings
from django.core.mail import send_mail
from django.shortcuts import render
from .forms import ContactForm
# Create your views here.

def home(request):
	title = "Contact Us"
	form = ContactForm(request.POST or None)
	confirm_message = None

	if form.is_valid():
		name = form.cleaned_data['name']
		comment = form.cleaned_data['message']
		sbj = "Contact Form"
		msg = 'From: %s \nMessage: %s' %(name, comment)
		frm = form.cleaned_data['email']
		to_us = [settings.EMAIL_HOST_USER]
		send_mail(sbj, msg, frm,
    		to_us, fail_silently=False)
		title = 'Thank you'
		confirm_message = "Thank you for your message."
		form = None
		
	context = {
			'title': title,
			'form': form,
			'confirm_message': confirm_message
		}

	return render(request, 'contact.html', context)