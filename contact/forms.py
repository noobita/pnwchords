from django import forms


class ContactForm(forms.Form):
	name = forms.CharField(max_length=120, required=True, 
		widget=forms.TextInput(
		attrs={'placeholder': 'Name', 'class':'form-control'}))
	email = forms.EmailField(required=True, 
		widget=forms.TextInput(
		attrs={'placeholder': 'Email', 'class':'form-control'})) #help_text='A valid email address, please.'
	message = forms.CharField(max_length=5000, required=True, 
		widget=forms.Textarea(
		attrs={'placeholder': 'Your Message', 'class':'form-control'}))
		#attrs={'class':'form-control', 'placeholder': 'Comment'}))
	#widget=forms.Textarea