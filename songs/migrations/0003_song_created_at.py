# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('songs', '0002_auto_20141110_0426'),
    ]

    operations = [
        migrations.AddField(
            model_name='song',
            name='created_at',
            field=models.DateTimeField(default=datetime.date(2014, 11, 10), auto_now_add=True),
            preserve_default=False,
        ),
    ]
