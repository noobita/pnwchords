# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('songs', '0010_artist_slug'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='artist',
            options={'ordering': ['-name']},
        ),
        migrations.AddField(
            model_name='song',
            name='tags',
            field=models.ManyToManyField(to='songs.Tag'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='tag',
            name='name',
            field=models.CharField(default='default', unique=True, max_length=200),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='artist',
            name='name',
            field=models.CharField(unique=True, max_length=100),
        ),
        migrations.AlterField(
            model_name='tag',
            name='slug',
            field=models.CharField(unique=True, max_length=200),
        ),
    ]
