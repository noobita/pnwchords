# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('songs', '0009_auto_20141110_0911'),
    ]

    operations = [
        migrations.AddField(
            model_name='artist',
            name='slug',
            field=models.CharField(default='default', max_length=100),
            preserve_default=False,
        ),
    ]
