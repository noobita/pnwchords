# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('songs', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='song',
            name='slug',
            field=models.CharField(default='test', unique=True, max_length=100),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='song',
            name='tag',
            field=models.CharField(max_length=60, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='song',
            name='views',
            field=models.IntegerField(default=0),
            preserve_default=True,
        ),
    ]
