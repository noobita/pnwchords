from django.conf.urls import patterns, include, url
from django.contrib import admin
from songs import views

urlpatterns = patterns('',

    # Examples:
    url(r'^$', views.HomeIndex.as_view(), name='home'),
    url(r'^artists/(?P<slug>[\w|\-]+)/$', views.ArtistDetailView.as_view(), name='artist_detail'),
    url(r'^artists/', views.ArtistIndex.as_view(), name='artist_index'),
    url(r'^tags/(?P<slug>[\w|\-]+)/$', views.TagDetailView.as_view(), name='tag_detail'),
    url(r'^search/', views.Search.as_view(), name='search'),
    url(r'^songs/', include('songs.urls')),

    url(r'^contact/', 'contact.views.home', name='contact'),
    url(r'^myprofile/$', views.UserDetail.as_view(), name='user_detail'),
    url(r'^edit/(?P<slug>[\w|\-]+)/$', views.SongUpdate.as_view(), name='song_update'),
    url(r'^transpose/', 'songs.views.transpose', name='transpose'),
    url(r'^admin/', include(admin.site.urls)),
    (r'^accounts/', include('allauth.urls')),
)
